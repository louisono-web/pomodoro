module Pomodoro exposing (..)

{-|

Simple pomodoro timer, see [pomodoro technique][pt] for details about this time 
management method.

[pt]:https://en.wikipedia.org/wiki/Pomodoro_Technique

-}


import Time exposing ( Posix, posixToMillis, millisToPosix, now )
import Positive as P exposing ( Positive )
import Task
import Html exposing ( Html, div, p, text )
import Html.Attributes exposing ( class )
import Browser exposing ( element )


{-| Record type to represent time durations. Only positive duration are allowed.
It is used to implement operations over `Posix` times.
-}
type alias Duration = { minutes: Positive, seconds: Positive, millis: Positive }


{-| Simple type to know what kind of period is ongoing for a `Pomodoro` value:  
- `Working` for values where a work period is ongoing  
- `Resting` otherwise
-}
type PeriodType = Working | Resting


{-| Record type to both represent a timer and contain all relevant information to 
apply the technique. Internals include:  
- `workInterval`: the duration of a work period
- `shortRestInterval`: the duration of a short rest period
- `longRestInterval`: the duration of the longer rest period
- `repeatCycle`: the number of work and rest cycles before the last longer rest period
- `startTime`: the time at which the current period started
- `elapsedTime`: the duration for which the current period has run
- `repeated`: the number of work period done so far
- `period`: the type of period currently ongoing
-}
type alias Pomodoro =
  { workInterval: Duration
  , shortRestInterval: Duration
  , longRestInterval: Duration
  , repeatCycle: Positive
  , startTime: Posix
  , elapsedTime: Duration
  , repeated: Positive
  , period: PeriodType
  }


{-| Difference between two `Posix` times. Using `timeDifference t1 t2` where `t1`
is a `Posix` time before `t2` results in a `Nothing`. Otherwise a `Just duration`
value will be returned.
-}
timeDifference: Posix -> Posix -> Maybe Duration
timeDifference t1 t2 =
  (posixToMillis t1) - (posixToMillis t2)
  |> P.posInt
  |> Maybe.map millisToDuration


{-| Adding a duration to a `Posix` time.
-}
timeSum: Posix -> Duration -> Posix
timeSum t1 duration =
  let
    millis = durationToMillis duration |> P.value
  in
  (posixToMillis t1) + millis
  |> millisToPosix


{- Positive constants used for duration conversion -}
thousand = P.prod P.ten P.hundred
sixtyThousand = P.prod (P.prod P.ten thousand) (P.sum P.five P.one)


{-| Compute the number of milliseconds in a `Duration` value.
-}
durationToMillis: Duration -> Positive
durationToMillis dur =
  P.prod dur.minutes sixtyThousand
  |> P.sum (P.prod dur.seconds thousand)
  |> P.sum dur.millis


{-| Create a `Duration` out of a positive number of minutes.
-}
minutesToDuration: Positive -> Duration
minutesToDuration minutes =
  { minutes = minutes, seconds = P.zero, millis = P.zero }


{-| Create a `Duration` out of a positive number of seconds.
-}
secondsToDuration: Positive -> Duration
secondsToDuration seconds =
  millisToDuration (P.prod seconds thousand)


{-| Create a `Duration` out of a positive number of milliseconds.
-}
millisToDuration: Positive -> Duration
millisToDuration millis =
  let
    minutes = P.div millis sixtyThousand
    seconds = P.div (P.remainderBy sixtyThousand millis) thousand
    remainingMillis = P.remainderBy thousand millis
  in
  { minutes = minutes, seconds = seconds, millis = remainingMillis }


{-| Return `True` if given `Pomodoro` is currently going through a work period,
`False` otherwise.
-}
working: Pomodoro -> Bool
working { period } =
  case period of
    Working -> True
    _ -> False


{-| Return the `Duration` for which the give `Pomodoro` value should run before
moving to the next period.
-}
currentInterval: Pomodoro -> Duration
currentInterval pomodoro =
  if working pomodoro
  then pomodoro.workInterval
  else
    if P.remainderBy pomodoro.repeatCycle pomodoro.repeated == P.zero
    then pomodoro.longRestInterval
    else pomodoro.shortRestInterval


{-| Compute the `Duration` left before moving to the next period.
-}
remainingTime: Pomodoro -> Duration
remainingTime pomodoro =
  P.sub (durationToMillis (currentInterval pomodoro)) (durationToMillis pomodoro.elapsedTime)
  |> Maybe.withDefault P.zero
  |> millisToDuration


{- default settings for pomodoro technique -}
defaultPomodoro =
  { workInterval = minutesToDuration (P.sum P.five (P.prod P.ten P.two))    -- 25minutes
  , shortRestInterval = minutesToDuration P.five                            -- 5minutes
  , longRestInterval = minutesToDuration (P.prod P.ten (P.sum P.one P.two)) -- 30minutes
  , repeatCycle = P.five
  , startTime = millisToPosix 0
  , elapsedTime = minutesToDuration P.zero                                  -- 0minutes
  , repeated = P.zero
  , period = Working
  }

{- for test purposes: default pomodoro with shorter times and cycle
defaultPomodoro =
  { workInterval = secondsToDuration (P.prod P.two P.ten)
  , shortRestInterval = secondsToDuration P.five
  , longRestInterval = secondsToDuration (P.prod P.five P.ten)
  , repeatCycle = P.sum (P.two P.one)
  , startTime = millisToPosix 0
  , elapsedTime = minutesToDuration P.zero
  , repeated = P.zero
  , period = Working
  }
-}


{-| Return a `Pomodoro` where the `startTime` field is the given `Posix` time. 
Useful to start a pomodoro timer from any `Pomodoro` that still has an uninitialized
`startTime`.
-}
initializePomodoroTime: Pomodoro -> Posix -> Pomodoro
initializePomodoroTime pomodoro time =
  { pomodoro | startTime = time }


{-| Advance internal clock of given `Pomodoro`. Depending on the time that has passed
between the given `Posix` and the `Pomodoro`'s `startTime`, the next period might
be started.
-}
tickPomodoroTime: Pomodoro -> Posix -> Pomodoro
tickPomodoroTime pomodoro time =
  let
    elapsed = 
      timeDifference time pomodoro.startTime 
      |> Maybe.withDefault (millisToDuration P.zero)
    currentInter = currentInterval pomodoro
  in
  if P.lessThan (durationToMillis elapsed) (durationToMillis currentInter)
  then { pomodoro | elapsedTime = elapsed }
  else
    let newTime = timeSum pomodoro.startTime currentInter
    in
    if working pomodoro
    then
      { pomodoro
      | repeated = P.sum pomodoro.repeated P.one
      , period = Resting
      , startTime = newTime
      , elapsedTime = millisToDuration (P.zero)
      }
    else
      { pomodoro
      | period = Working
      , startTime = newTime
      , elapsedTime = millisToDuration (P.zero)
      }


{- CSS classes -}
classResting = class "resting"
classWorking = class "working"
classPomodoro = class "pomodoro-timer"
classPeriods = class "periods"
classDone = class "done"
classTodo = class "todo"


{-| Message type to signal desirable update to the `Pomodoro` value used as the
app's model. Two kinds of message are possible:
- `Tick Posix`: signals that the `Pomodoro`'s clock should be updated
- `Init Posix`: signals that the `Pomodoro`'s start time should be updated
-}
type Msg =
  Tick Posix
  | Init Posix


{-| Initial state of the app (indepent of anything else):
- a `Pomodoro` that doesn't really have a `startTime` 
- the command to initialize that `Pomodoro` with the current time
-}
init: () -> (Pomodoro, Cmd Msg)
init _ =
  ( defaultPomodoro, Task.perform Init now )


{-| Update given `Pomodoro` depending on the content of the given `Msg` by leveraging
`initializePomodoroTime` and `tickPomodoroTime`.
-}
update: Msg -> Pomodoro -> ( Pomodoro, Cmd Msg )
update msg pomodoro =
  let
    updatedPomodoro =
      case msg of
        Init time -> initializePomodoroTime pomodoro time
        Tick time -> tickPomodoroTime pomodoro time
  in
  ( updatedPomodoro, Cmd.none )


{-| Render given `Duration` into a html paragraph element.
-}
viewTime: Duration -> Html Msg
viewTime duration =
  let (minutes, seconds) = (duration.minutes, duration.seconds)
  in
  String.fromInt (P.value minutes) ++ "'" ++ String.fromInt (P.value seconds)
  |> (\timeStr -> p [ ] [ text timeStr ])


{-| Render the amount of work periods done over a given `Pomodoro`'s `repeatCycle`
by means of html div elements.
-}
viewPeriods: Pomodoro -> Html Msg
viewPeriods pomodoro =
  let 
    list =
      if not (working pomodoro) && P.equal (P.remainderBy pomodoro.repeatCycle pomodoro.repeated) P.zero
      then List.repeat (P.value pomodoro.repeatCycle) (div [ classDone ] [])
      else
        let
          modRepeated = 
            P.remainderBy pomodoro.repeatCycle pomodoro.repeated
            |> P.value
        in
        List.repeat (P.value pomodoro.repeatCycle) Nothing
        |> List.indexedMap (\i _ -> div [ (if i < modRepeated then classDone else classTodo) ] [])
  in
  div [ classPeriods ] list


{-| Render given `Pomodoro` by leveraging `viewTime` and `viewPeriods`.
-}
view: Pomodoro -> Html Msg
view pomodoro =
  let
    classPeriod =
      if working pomodoro
      then classWorking
      else classResting
  in
  div 
    [ classPomodoro, classPeriod ]
    [ viewTime (remainingTime pomodoro), viewPeriods pomodoro ]


{-| Subscriptions for the pomodoro timer: send a `Tick Posix` message every 100
milliseconds
-}
subs: Pomodoro -> Sub Msg
subs _ = Time.every 100 Tick


{-| Main
-}
main =
  element
    { init = init
    , update = update
    , view = view
    , subscriptions = subs
    }
