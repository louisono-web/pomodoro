module Positive exposing 
  ( Positive, posInt, value
  , sum, sub, prod
  , div, remainderBy, lessThan 
  , greaterThan, equal, zero
  , one, two, five
  , ten, twenty, fifty
  , hundred 
  )


{-| Positive integer values (>= 0). Operations over `Positive` values are enforcing
positive integer values which makes them safe to use in situations where negative
values should never be encountered.
-}
type Positive = Positive Int


{-| Constructor for `Positive` values. If the given `Int` is not positive then
`Nothing` is returned, otherwise a `Just Positive` is returned.
-}
posInt: Int -> Maybe Positive
posInt i =
  if i < 0 then Nothing else Just (Positive i)


{-| Return the sum of two `Positive` values.
-}
sum: Positive -> Positive -> Positive
sum posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> Positive (i1 + i2)


{-| Compute the difference between two `Positive` values. If the difference is 
negative then `Nothing` is returned, else a `Just Positive` is returned.
-}
sub: Positive -> Positive -> Maybe Positive
sub posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> posInt (i1 - i2)


{-| Return the product of two `Positive` values.
-}
prod: Positive -> Positive -> Positive
prod posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> Positive (i1 * i2)


{-| Return the division of two `Positive` values.
-}
div: Positive -> Positive -> Positive
div posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> Positive (i1 // i2)


{-| Return the remainder of the division of two `Positive` values.
-}
remainderBy: Positive -> Positive -> Positive
remainderBy posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> Positive (Basics.remainderBy i1 i2)


{-| Return `True` if the first `Positive` is (strictly) less than the second
`Positive`.
-}
lessThan: Positive -> Positive -> Bool
lessThan posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> i1 < i2


{-| Return `True` if the first `Positive` is (strictly) greater than the second
`Positive`.
-}
greaterThan: Positive -> Positive -> Bool
greaterThan posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> i1 > i2


{-| Return `True` if both `Positive` values are the same.
-}
equal: Positive -> Positive -> Bool
equal posI1 posI2 =
  case (posI1, posI2) of
    (Positive i1, Positive i2) -> i1 == i2


{-| Return the underlying `Int` of given `Positive`.
-}
value: Positive -> Int
value pos =
  case pos of
    Positive i -> i


{- built-in `Positive` values -}

{-| `Positive 0`
-}
zero: Positive
zero = Positive 0


{-| `Positive 1`
-}
one: Positive
one = Positive 1


{-| `Positive 2`
-}
two: Positive
two = Positive 2


{-| `Positive 5`
-}
five: Positive
five = Positive 5


{-| `Positive 10`
-}
ten: Positive
ten = Positive 10


{-| `Positive 20`
-}
twenty: Positive
twenty = Positive 20


{-| `Positive 50`
-}
fifty: Positive
fifty = Positive 50


{-| `Positive 100`
-}
hundred: Positive
hundred = Positive 100
