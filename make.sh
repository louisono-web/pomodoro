#! /usr/bin/env bash
THIS_DIR="$(dirname $0)"
SRC="$THIS_DIR/src/Pomodoro.elm"
TARGET="$THIS_DIR/pomodoro.js"

elm make $SRC --output $TARGET
