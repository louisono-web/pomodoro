# Pomodoro

A simple elm-based implementation of a [Pomodoro technique](https://en.wikipedia.org/wiki/Pomodoro_Technique) app.

## Build

Run the `make.sh` script to build the project. After doing so the transpiled-to-js file
`pomodoro.js` should be located in the same folder as the `index.html` page.
